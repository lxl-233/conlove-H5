// common.js
export const tabbar = [{
		pagePath: "/pages/home/home",
		iconPath: "/static/tabar-image/home.png",
		selectedIconPath: "/static/tabar-image/home-select.png",
		text: "缘来",
		customIcon: false,
	},
	{
		pagePath: "/pages/square/square",
		iconPath: "/static/tabar-image/square.png",
		selectedIconPath: "/static/tabar-image/square-select.png",
		text: "小宇宙",
		customIcon: false,
	},
	{
		
		customIcon: false,
		pagePath: "/pages/release/add",
		iconPath: "/static/tabar-image/jia.png",
		selectedIconPath: "/static/tabar-image/jia-select.png",
		midButton: false,
		text: '发布',
	},
	{
		pagePath: "/pages/message/message",
		iconPath: "/static/tabar-image/message.png",
		selectedIconPath: "/static/tabar-image/message-select.png",
		text: "消息",
		customIcon: false,
		count: 0,
	},
	{
		pagePath: "/pages/user/user",
		iconPath: "/static/tabar-image/user.png",
		selectedIconPath: "/static/tabar-image/user-select.png",
		text: "我的",
		customIcon: false,
	},
]


