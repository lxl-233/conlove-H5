import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import {tabbar} from 'common/tabbar.js'
const store = new Vuex.Store({
    state: {
		token: uni.getStorageSync('token') ? uni.getStorageSync('token') : '',
		userDB: uni.getStorageSync("user") ? JSON.parse(uni.getStorageSync("user")) : {},
		tabbar: tabbar
	},
    actions: {
		set({state}, data) {
		  // 把学校和认证学校 更新
		  state.userDB = data.user;
		  if(data.token != null) {
			  state.token = data.token;
			  uni.setStorageSync('token', data.token);
		  }
		  uni.setStorageSync('user', JSON.stringify(data.user));
		},
		del({state}) {
		  state.token = '';
		  state.userDB = {};
		  if(uni.getStorageSync('token')) {
			  uni.removeStorageSync('token');
		  }
			
		  if(uni.getStorageSync("user")) {
			uni.removeStorageSync('user');
		  }
		},
		updateMessage({state}, num) {
			state.tabbar[3].count = num 
		}
	},
	getters: {
		isLogin(state) {
		  return JSON.stringify(state.userDB) !== "{}";
		}
	}
})
export default store